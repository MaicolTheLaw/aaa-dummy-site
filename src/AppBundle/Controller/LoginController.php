<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Form\Type\LoginType;
use AppBundle\Entity\User;

class LoginController extends Controller
{
    /**
     * @Route("/login", name="get_login")
     * @Method("GET")
     */
    public function getLoginAction()
    {
        $user = new User();
        $form = $this->createForm(LoginType::class, $user, [
            'action' => $this->generateUrl('post_login'),
            'method' => 'POST',
        ]);

        return $this->render('login/login.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/login", name="post_login")
     * @Method("POST")
     */
    public function postLoginAction(Request $request)
    {
        $user = new User();

        $form = $this->createForm(LoginType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('homepage');
        }

        return new Response("Bad Request.", Response::BAD_REQUEST);
    }
}
